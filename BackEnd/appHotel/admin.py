from django.contrib import admin
from .models.hotel import Hotel
from .models.habitacion import Habitacion
from .models.usuario import User
from .models.reserva import Reserva

admin.site.register(Hotel)
admin.site.register(Habitacion)
admin.site.register(User)
admin.site.register(Reserva)



# Register your models here.
