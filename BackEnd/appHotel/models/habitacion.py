from django.db import models
from .hotel import Hotel

class Habitacion(models.Model):
    id = models.AutoField(primary_key=True)
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    descripcion = models.CharField('descripcion',max_length=200)
    capacidad = models.IntegerField('capacidad')
    precio = models.FloatField('precio')