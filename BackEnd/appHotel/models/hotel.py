from django.db import models
class Hotel(models.Model):
    id = models.AutoField(primary_key=True)
    puntuacion = models.FloatField('puntuacion',null=True)
    ciudad = models.CharField('ciudad', max_length=40 , null=False)
    descripcion = models.CharField('descripcion', max_length=200 , null=False)
    servicios_incluidos = models.CharField('servicios_incluidos', max_length=200, null=True)
