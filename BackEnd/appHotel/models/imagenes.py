from django.db import models
from datetime import datetime
from .hotel import Hotel
from .habitacion import Habitacion 
import random
def nameFile(instance, filename):
    todays_date = datetime.now()
    extension = "." + filename.split('.')[-1]
    return '/'.join(['img',str(todays_date.year)+"-"+str(todays_date.month)+"-"+str(todays_date.day)+"-"+str(todays_date.hour)+"-"+str(todays_date.minute)+"-"+str(random.randint(10,999))+extension])

class img_hotels(models.Model):
    id = models.AutoField(primary_key=True)
    hotel = models.ForeignKey(Hotel,related_name="hotel_id",on_delete=models.CASCADE)
    imagen = models.ImageField(upload_to=nameFile)

class img_habitacion(models.Model):
    id = models.AutoField(primary_key=True)
    habitacion = models.ForeignKey(Habitacion,related_name="habitacion_id",on_delete=models.CASCADE)
    imagen = models.ImageField(upload_to=nameFile)

