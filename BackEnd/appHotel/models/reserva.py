from django.db import models
from .usuario import User
from .habitacion import Habitacion

class Reserva(models.Model):
    id = models.AutoField(primary_key=True)
    habitacion = models.ForeignKey(Habitacion, on_delete=models.DO_NOTHING)
    usuario = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=False, blank=False)
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()