from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password


class UserManager(BaseUserManager):
    def create_user(self, correo, password=None):
        """Creates and saves a user with the given username and password."""
        if not correo:
            raise ValueError('Users must have an username')
        user = self.model(correo=correo)
        user.set_password(password)
        user.save(using=self._db)
        return user
    def create_superuser(self, correo, password):
        """Creates and saves a superuser with the given username and password."""
        user = self.create_user(
        correo=correo,
        password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    nombre = models.CharField('nombre', max_length = 30)
    apellido = models.CharField('apellido', max_length = 30)
    edad = models.IntegerField('edad')
    telefono = models.BigIntegerField('telefono')
    password = models.CharField('Password', max_length = 256)
    tipo_doc_id = models.CharField('tipo_doc_id', max_length = 30)
    num_doc_id = models.BigIntegerField('num_doc_id')
    correo = models.EmailField('correo', max_length = 100, unique=True)
    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)
    objects = UserManager()
    USERNAME_FIELD = 'correo'