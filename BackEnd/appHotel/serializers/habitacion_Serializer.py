from rest_framework import serializers
from appHotel.models.habitacion import Habitacion
class HabitacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Habitacion
        fields = ['id','hotel','descripcion', 'precio', 'capacidad']