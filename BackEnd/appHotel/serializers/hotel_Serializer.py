from rest_framework import serializers
from appHotel.models.hotel import Hotel
from appHotel.models.imagenes import img_hotels
from appHotel.serializers.imagen_Serializer import ImagenHotelSerializer

class HotelSerializer(serializers.ModelSerializer):
    # hotel_imagenes = ImagenHotelSerializer(many=True)
    class Meta:
        model = Hotel
        fields = ['id', 'puntuacion', 'ciudad', 'descripcion', 'servicios_incluidos']
    # def create(self, validated_data):
    #         imagenes = validated_data['hotel_imagenes']
    #         del validated_data['hotel_imagenes']
    #         hotel = Hotel.objects.create(**validated_data)
    #         for imagen in imagenes:
    #            #print(dict(imagen.items))
    #             img_hotels.objects.create(hotel=hotel, **imagen)
    #         return hotel