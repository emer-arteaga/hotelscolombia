from rest_framework import serializers
from appHotel.models.imagenes import img_hotels
from appHotel.models.imagenes import img_habitacion

class ImagenHotelSerializer(serializers.ModelSerializer):
    class Meta:
        model= img_hotels
        fields = ["id","imagen", "hotel"]
    # def create(self, validated_data):
    #     # print('valid_data', validated_data)
    #     # imagen = validated_data['imagen']
    #     # for img in imagen:
    #     #     img_hotels.objects.create(imagen=img,hotel=1)
    #     # return Response({"creado"},status=status.HTTP_201_CREATED)
    #     return super().create(validated_data)
class ImagenHabitacionSerializer(serializers.ModelSerializer):
    class Meta:
        model= img_habitacion
        fields = ["id","imagen", "habitacion"]
    # def create(self, validated_data):
    #     print(validated_data)
    #     imagen = validated_data
    #     img_habitacion.objects.create(imagen=imagen)
    #     return super().create(validated_data)