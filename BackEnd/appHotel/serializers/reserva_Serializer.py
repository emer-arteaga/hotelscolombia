from appHotel.models.reserva import Reserva
from appHotel.models.habitacion import Habitacion
from appHotel.models.usuario import User
from rest_framework import serializers
class reserva_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Reserva
        fields = ['id', 'fecha_inicio', 'fecha_fin','usuario','habitacion']