from rest_framework import serializers
from appHotel.models.usuario import User
#from appHotel.models.reserva import Reserva
#from appHotel.serializers.reserva_Serializer import reserva_Serializer

class UsuarioSerializer(serializers.ModelSerializer):
    #Reserva = reserva_Serializer()
    class Meta:
        model = User
        fields = ['id', 'nombre', 'apellido', 'correo', 'telefono', 'password', 'edad', 'tipo_doc_id', 'num_doc_id']