from rest_framework import status, views
from rest_framework.response import Response
from appHotel.models.imagenes import img_hotels
from appHotel.serializers.imagen_Serializer import ImagenHotelSerializer
from appHotel.models.imagenes import img_habitacion
from appHotel.serializers.imagen_Serializer import ImagenHabitacionSerializer

def modify_input_for_multiple_files(ids,image):
    dict = {}
    dict['habitacion'] = ids
    dict['imagen'] = image
    return dict
def multiples_imagenes_hotel(ids,image):
    dict = {}
    dict['hotel'] = ids
    dict['imagen'] = image
    return dict
    
class HotelImagenView(views.APIView):
    queryset = img_hotels.objects.all()
    def post(self, request, *args, **kwargs):
        # converts querydict to original dict
        id_hotel = request.data['hotel']
        images = dict((request.data).lists())['imagen']
        print(images)
        flag = 1
        arr = []
        for img_name in images:
            modified_data = multiples_imagenes_hotel(id_hotel,img_name)
            file_serializer = ImagenHotelSerializer(data=modified_data)
            if file_serializer.is_valid():
                file_serializer.save()
                arr.append(file_serializer.data)
            else:
                flag = 0
        if flag == 1:
            return Response(arr, status=status.HTTP_201_CREATED)
        else:
            return Response({"mensaje": "Algo salio mal intentalo nuevamente"}, status=status.HTTP_400_BAD_REQUEST)
class HabitacionImagenView(views.APIView):
    # queryset = img_hotels.objects.all()
    def post(self, request, *args, **kwargs):
        # converts querydict to original dict
        idhabitacion = request.data['habitacion']
        images = dict((request.data).lists())['imagen']
        print(images)
        flag = 1
        arr = []
        for img_name in images:
            modified_data = modify_input_for_multiple_files(idhabitacion,img_name)
            file_serializer = ImagenHabitacionSerializer(data=modified_data)
            if file_serializer.is_valid():
                file_serializer.save()
                arr.append(file_serializer.data)
            else:
                flag = 0
        if flag == 1:
            return Response(arr, status=status.HTTP_201_CREATED)
        else:
            return Response({"mensaje": "Algo salio mal intentalo nuevamente"}, status=status.HTTP_400_BAD_REQUEST)
    