from rest_framework import status, views
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from appHotel.models.habitacion import Habitacion
from appHotel.serializers.habitacion_Serializer import HabitacionSerializer

class HabitacionCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = HabitacionSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)