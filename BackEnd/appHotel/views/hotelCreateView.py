from rest_framework import status, views
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from appHotel.models.hotel import Hotel
from appHotel.serializers.hotel_Serializer import HotelSerializer

class HotelCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = HotelSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)