from rest_framework import status, views
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from appHotel.models.reserva import Reserva
from appHotel.serializers.reserva_Serializer import reserva_Serializer

class ReservaCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = reserva_Serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)