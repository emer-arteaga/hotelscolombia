from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response

from appHotel.models.reserva import Reserva
from appHotel.serializers.reserva_Serializer import reserva_Serializer

class reservaDetallesView(generics.RetrieveAPIView):
    queryset = Reserva.objects.all()
    serializer_class = reserva_Serializer
    def get(self, request, *args, **kwargs):
        
        return Response(status=status.HTTP_200_OK)