from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from appHotel.models.usuario import User
from appHotel.serializers.usuario_Serializer import UsuarioSerializer

class UsuarioDetallesView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UsuarioSerializer
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        print(valid_data)
        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail': 'No autorizado'}
            return Response(stringResponse,status=status.HTTP_401_UNAUTHORIZED)
        return super().get(request, *args, **kwargs)
class UsuarioIdDetalles(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UsuarioSerializer
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        return Response({'id': valid_data['user_id']}, status=status.HTTP_200_OK)