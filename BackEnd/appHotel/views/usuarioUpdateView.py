from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend
from appHotel.models.usuario import User
from appHotel.serializers.usuario_Serializer import UsuarioSerializer

class UsuarioUpdateView(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()   
    serializer_class=UsuarioSerializer
    permission_classes = (IsAuthenticated,)
        
    def put(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token, verify=False)
        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail': 'No autorizado, solo el propietario de la cuenta puede actualizar su cuenta'}
            return Response(stringResponse,status=status.HTTP_401_UNAUTHORIZED)
        return super().put(request, *args, **kwargs)