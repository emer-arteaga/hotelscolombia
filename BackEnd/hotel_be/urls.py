"""hotel_be URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from appHotel import views

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/id/', views.UsuarioIdDetalles.as_view()),
    path('reserva/', views.ReservaCreateView.as_view()),
    path('hotel/', views.HotelCreateView.as_view()),
    path('habitacion/', views.HabitacionCreateView.as_view()),
    path('user/<int:pk>/', views.UsuarioDetallesView.as_view()),
    path('user/update/<int:pk>/', views.UsuarioUpdateView.as_view()),
    path('user/delete/<int:pk>/', views.UsuarioDestroyView.as_view()),
    path('imagen/habitacion/', views.HabitacionImagenView.as_view()),
    path('imagen/hotel/', views.HotelImagenView.as_view()),

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root= settings.MEDIA_ROOT)
